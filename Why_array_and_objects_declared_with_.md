## Why arrays and objects declared with the 'const' Keyword can be modified?

1. In the case of numbers, strings and booleans with a const keyword, we can not reassign a const variable. Array and object also can not reassign.
2. In the case of objects and arrays, they have methods and properties that let you modify the object or array.

        for example:- const arr=[1,2,3];
        arr.push(5)
        console.log(arr);
        The output would be : [1,2,3,5]

        we can push, pop and modify the element inside the array declared with the const keyword.

        for example:- const arr=[1,2,3]
        arr[0]=5;
        The output will be : [5,2,3]

### We can also modify the object declare with the 'const' keyword.
1. We can assign new property to a object declare with 'const' keyword.
   
        for example:- const obj={
                                name:"xyz"
                                }
        In this example we can add a new property like,
                            obj.place="Bangalore";
        After adding the object will look like : {name:"xyz", place:"banglore"}

        We can also modify the property like, obj.name="abc"
        After the object will look like, obj={name:"abc" , place:"banglore"}

2. 'Const' keyword does not stop array and objects from being modified, It only stops them from being reassigned.

## Summary

You can not modify simple variables like numbers, strings, or boolean using 'const' keyword. But, objects and arrays provided an additional method that let modify their value.
